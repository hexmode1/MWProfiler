<?php
/**
 * Generate JSON for d3 flamegraph
 *
 * PHP Version 7+
 *
 * Copyright (C) 2023  NicheWork, LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author  Mark A. Hershberger <mah@everybody.org>
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GPL v3
 * @link    https://gitlab.com/hexmode1/MWProfiler
 */

require_once __DIR__ . '/vendor/autoload.php';

use Hexmode\MwProfiler\Handler;

$handler = new Handler(__DIR__);

$page = $handler->getPage();

$page->setupHeaders();
echo $page->getJson($handler->getDataParam());
