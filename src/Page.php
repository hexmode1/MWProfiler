<?php

/**
 * Output data for a page
 *
 * PHP Version 7+
 *
 * Copyright (C) 2023  NicheWork, LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author  Mark A. Hershberger <mah@everybody.org>
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GPL v3
 * @link    https://gitlab.com/hexmode1/MWProfiler
 */

namespace Hexmode\MwProfiler;

use Dotenv\Dotenv;
use mysqli as DB;
use mysqli_result as DBResult;

class Page {
    private string $name;
    private int $id;
    private int $depth;
    private array $stack;
    private array $statement;
    private array $types = [
        "ct", "wt", "excl_wt", "ut", "excl_ut", "cpu", "excl_cpu", "mu",
        "excl_mu", "pmu", "excl_pmu"
    ];
    private array $pageStack;
    private string $dbServer;
    private string $dbUser;
    private string $dbPass;
    private string $dbName;
    private DB $db;

    const endpoints = 0;
    const fullStack = 1;
    const pageId = 2;

    public function __construct(
        string $name,
        string $dbServer,
        string $dbUser,
        string $dbPass,
        string $dbName
    ) {
        $this->name = $name;
        $this->dbServer = $dbServer;
        $this->dbUser = $dbUser;
        $this->dbPass = $dbPass;
        $this->dbName = $dbName;
        $this->depth  = 0;
        $this->db = $this->getConnection();
        $this->statement = $this->prepareStatements();
        $this->id = $this->getPageId($name);
        $this->pageStack = $this->getPageStackFromDB();
    }

    public function getConnection(): DB {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $db = new DB(
            $this->dbServer, $this->dbUser, $this->dbPass, $this->dbName
        );
        $db->set_charset('utf8mb4');
        return $db;
    }

    protected function prepareStatements(): array {
        $types = implode(", ", $this->types);
        return [
            self::endpoints => $this->db->prepare(<<<SQL
                  SELECT id, page, caller, callee, $types
                    FROM combo
                   WHERE callee IS NULL
                     AND page=?
                  SQL),
            self::fullStack => $this->db->prepare(<<<SQL
                  SELECT id, page, caller, callee, $types
                    FROM combo
                   WHERE page=?
                  SQL),
            self::pageId => $this->db->prepare(<<<SQL
                  SELECT id, title
                    FROM page
                   WHERE title = ?
                  SQL),
        ];
    }

    protected function getPageId(string $name): int {
        $this->statement[self::pageId]->bind_param('s',$name);
        $this->statement[self::pageId]->execute();
        $res = $this->statement[self::pageId]->get_result();
        if ( $res->num_rows !== 1 ) {
            throw new Exception("No such page: $name");
        }

        $row = $res->fetch_array();
        if ( !is_array($row) || !isset($row['id']) ) {
            throw new Exception("Shouldn't get here, error fetching: $name");
        }

        return $row['id'];
    }

    private function getPageStackFromDB(): array {
        $this->statement[self::fullStack]->bind_param('i', $this->id);
        $this->statement[self::fullStack]->execute();
        $res = $this->statement[self::fullStack]->get_result();
        return $res->fetch_all(MYSQLI_ASSOC);
    }

    private function extractCalls(string $caller): array {
        $calls = array_filter($this->pageStack,
                              function(array $slice) use ($caller) {
                                  if ($slice['caller'] === $caller) {
                                      return true;
                                  }
                                  return false;
                              }
        );
        $this->pageStack = array_diff_key( $this->pageStack, $calls );
        return $calls;
    }

    private function getMain( array &$mainCalls ): array {
        $main = array_filter($mainCalls, function($slice) {
            if ($slice['callee'] === null) {
                return true;
            }
            return false;
        });
        $id = array_keys($main)[0];
        unset($mainCalls[$id]);

        return $main[$id];
    }

    public function getCallStacks(array $calls, string $type): array {
        $row = [];

        $ignore = [
            "AmazonS3Hooks::setup", "Language::getMagic",
            "CLIParser::initParser", "Title::newFromText",
            "MediaWiki\\HookContainer\\HookRunner::onParserAfterParse"
        ];
        $ignore = [];
        $this->depth++;
        // Depth is set to 90 here so that we don't time out.
        if ($this->depth > 90) {
            return $row;
        }
        foreach( $calls as $call ) {
            $callee = $call['callee'];
            if (in_array($callee, $ignore)) {
                continue;
            }
            $calleeCalls = $this->extractCalls($callee);

            $row[] = [
                'name'     => $callee,
                'value'    => $call[$type],
                'children' => $this->getCallStacks($calleeCalls, $type)
            ];
        }
        $this->depth--;
        return $row;
    }

    /**
     * Get a stack for a particular data type
     *
     * @param string $type One of ct, wt, excl_wt, ut, excl_ut, cpu,
     * excl_cpu, mu, excl_mu, pmu, excl_pmu
     */
    private function getStack(string $type): array {
        $mainCalls = $this->extractCalls('main()');
        $main = $this->getMain($mainCalls);
        return [
            'name'     => $this->name,
            'value'    => $main[$type],
            'children' => $this->getCallStacks($mainCalls, $type)
        ];
    }

    private function resToStackSlice(
        DBResult $res,
        string $caller,
        string $type
    ): array {
        // If there are no more results, then return an empty array
        // indicating the caller didn't call anyone.
        $data = $res->fetch_assoc();
        if ( $data === null ) {
            return [];
        }

        return [
            'name'     => $caller,
            'value'    => $data[$type],
            'children' => $this->getChildrenOfResult($data, $type),
        ];
    }

    /**
     * Get an array of each child's call stack for the parent function.  If
     * the parent didn't call any functions, it will have no children.
     */
    private function getChildrenOfResult(array $data, string $type): array {
        $children = [];
        $caller = $data['callee'];
        if($caller === null) {
            return [];
        }

        $this->statement[self::children]->bind_param('si', $caller, $this->id);
        $this->statement[self::children]->execute();
        $res = $this->statement[self::children]->get_result();

        $data = $this->resToStackSlice($res, $caller, $type);
        while(count($data) !== 0) {
            $children[] = $data;
            $data = $this->resToStackSlice($res, $caller, $type);
        }
        return $children;
    }

    public function getJson(string $type): string {
        if ( !in_array( $type, $this->types ) ) {
            throw new Exception("The type '$type' is not valid.");
        }

        return json_encode($this->getStack($type)) . "\n";
    }

    public function setupHeaders(): void {
        header("Content-type: application/json; charset=utf-8");
    }
}
