<?php

/**
 * Handle fetching from DB
 *
 * PHP Version 7+
 *
 * Copyright (C) 2023  NicheWork, LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author  Mark A. Hershberger <mah@everybody.org>
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GPL v3
 * @link    https://gitlab.com/hexmode1/MWProfiler
 */

namespace Hexmode\MwProfiler;

use Dotenv\Dotenv;

class Handler {
    private Dotenv $env;

    public function __construct(string $dir) {
        $this->env = Dotenv::createImmutable($dir);

        $this->env->load();
        $this->env->required('DB_SERVER')->notEmpty();
        $this->env->required('DB_USER')->notEmpty();
        $this->env->required('DB_PASS')->notEmpty();
        $this->env->required('DB_NAME')->notEmpty();
    }

    public function getPage(): Page {
        return new Page(
            $this->getPageName(),
            $_ENV['DB_SERVER'],
            $_ENV['DB_USER'],
            $_ENV['DB_PASS'],
            $_ENV['DB_NAME'],
        );
    }

    protected function getPageName(): string {
        return $_GET['page'] ?? $_ENV['page'] ?? 'Main_Page';
    }

    public function getDataParam(): string {
        return $_GET['data'] ?? $_ENV['data'] ?? 'wt';
    }
}

